# Grafana dashboards of the ProfiT-HPC toolkit

### Grafana

The dashboards were developed using Grafana V7.0.5

### Installation

1. Import the dashboards (*.json files) in Grafana
2. Change the links in tables to the path assigned by Grafana after the import
